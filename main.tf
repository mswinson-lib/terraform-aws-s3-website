data "aws_route53_zone" "site" {
  name = "${var.zone_name}."
}

resource "aws_s3_bucket" "site" {
  bucket = "${var.bucket_name}.${var.zone_name}"
  acl = "public-read"

  website {
    index_document = "index.html"
    error_document = "error.html"
  }

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["GET"]
    allowed_origins = ["*"]
    max_age_seconds = 3000
  }

  provider = "aws"
}

resource "aws_s3_bucket_object" "public" {
  key = "public/"
  bucket = "${aws_s3_bucket.site.id}"
  acl = "public-read"
  source = "/dev/null"

  provider = "aws"
}


resource "aws_s3_bucket_object" "index" {
  key = "index.html"
  acl = "public-read"
  bucket = "${aws_s3_bucket.site.id}"
  source = "${path.module}/files/index.html"
  content_type = "text/html"
  etag =  "${md5(file("${path.module}/files/index.html"))}"

  provider = "aws"
}

resource "aws_s3_bucket_object" "error" {
  key = "index.html"
  acl = "public-read"
  bucket = "${aws_s3_bucket.site.id}"
  source = "${path.module}/files/error.html"
  content_type = "text/html"
  etag =  "${md5(file("${path.module}/files/error.html"))}"

  provider = "aws"
}


resource "aws_route53_record" "site" {
  zone_id = "${data.aws_route53_zone.site.zone_id}"
  name = "${var.bucket_name}"
  type = "A"

  alias {
    name = "${aws_s3_bucket.site.website_domain}"
    zone_id = "${aws_s3_bucket.site.hosted_zone_id}"
    evaluate_target_health = true
  }
}

