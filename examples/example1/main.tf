provider "aws" {
  region = "us-east-1"
}

module "example1" {
  source = "../../"

  zone_name = "dev.mswinson.com"
  bucket_name = "example1"
}
