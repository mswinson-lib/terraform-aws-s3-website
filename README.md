# terraform-aws-s3-website

[Terraform Module]() for creating an s3 static website

## Features


## Usage

```HCL
module "mysite" {
  source = "bitbucket.org/mswinson-lib/terraform-aws-s3-website"

  bucket_name = "www"   # default
  zone_name = ""        # required
}
```

## Inputs

| Name | Description | Type | Default | Required
| bucket_name | bucket name | string | www | true
| zone_name | zone name | string | | true


## Outputs

None

